import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.4

Rectangle {
    color: "pink"
    property StackView stackView;

    Text {
        text: "Account"
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (!stackView.busy) {
                stackView.pop();
            }
        }
    }
}

