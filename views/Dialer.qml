import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.4

Rectangle {
    color: "yellow"
    property StackView stackView;

    Text {
        text: "Dialer"
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            stackView.pop();
        }
    }
}

