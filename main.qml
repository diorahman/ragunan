import QtQuick 2.4
import QtQuick.Window 2.0
import QtQuick.Controls 1.3

import "views"

ApplicationWindow {
    visible: true
    width: 800
    height: 480

    Rectangle {
        color: "#212126"
        anchors.fill: parent
    }

    ListModel {
        id: pageModel

        ListElement {
            title: "Account"
            page: "views/Account.qml"
        }

        ListElement {
            title: "Dialer"
            page: "views/Dialer.qml"
        }

        ListElement {
            title: "Setting"
            page: "views/Setting.qml"
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        focus: true
        initialItem: Item {
            width: parent.width
            height: parent.height

            ListView {
                id: listView
                model: pageModel
                width: pageModel.count * 100
                height: 100
                anchors.centerIn: parent
                orientation: ListView.Horizontal
                spacing: 10
                delegate: Rectangle {
                    height: 100
                    width: 100
                    Text {
                        text: title
                        anchors.centerIn: parent
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (!stackView.busy) {
                                stackView.push({
                                    item : Qt.resolvedUrl(page),
                                    properties: { stackView: stackView }
                                });
                            }
                        }
                    }
                }
            }
        }
    }

}
